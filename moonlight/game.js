var config = {
    type: Phaser.WEBGL,
    width: 1520,
    height: 780,
    backgroundColor: '#2d2d2d',
    parent: 'phaser-example',
    physics: {
        default: 'arcade',
        arcade: {
            gravity: { y: 0 }
        }
    },
    scene: {
        preload: preload,
        create: create,
        update: update
    }
};

var game = new Phaser.Game(config);
/*=Herní proměnné=*/
var hp = 20;
var obrana = 0;
var utok = 4;
var money = 6;
var exp = 2;
var dogma = 15;
/*=Ovládaní mapy=*/
var cam;
var map;
var zmenpatro;
var patro = 0;
var doors;
var getout;
var element;
var cursors;
var player;
var senkyrka;
var npc;
var keyW;
var keyD;
var keyA;
var keyS;
var dia = true;
var panciricon;
var showDebug = false;
/*dialogy trigger*/
var dia1;
var dia2;
var visitvezeni = 0;
var visithospoda = 0;
var meetlocalmen = 0;
var meetsenkyrka = 0;
var meetwatchmen = 0;
var ctrl = true;
var cutscene = false;
var promenaY = -300;
var zlom = 0;

function preload ()
{
    draw();
    this.load.image('map-part1', 'Assets/map-part1.png');
    this.load.image('map-part2', 'Assets/map-part2.png');
    this.load.image('map-part3', 'Assets/map-part3.png');
    this.load.image('map-part4', 'Assets/map-part4.png');
    this.load.image('map-part5', 'Assets/map-part5.png');
    this.load.image('map-part6', 'Assets/map-part6.png');
    this.load.image('map-part7', 'Assets/map-part7.png');
    this.load.image('map-part8', 'Assets/map-part8.png');
    this.load.image('map-part9', 'Assets/map-part9.png');
    this.load.image('bakeryfasade','Assets/bakery-fasade-m.png');
    this.load.image('post-office','Assets/post-office-m.png');
    this.load.image('farm','Assets/statek.png');
    this.load.image('stodola','Assets/stodola.png');
    this.load.image('kovarna','Assets/kovarna.png');
    this.load.image('pancir-icon','Assets/pancir-icon.png');
    this.load.image('bariera','Assets/bariera3.png');
    this.load.image('rotace','Assets/door.png');
    this.load.image('radnicefasada','Assets/radnice-fasada-m.png');
    this.load.image('hospodafasada','Assets/hospoda-fasada2-m2.png');
    this.load.image('door','Assets/door2.png');
    this.load.image('doorred','Assets/door.png');
    this.load.image('pravafasada','Assets/hospoda-fasada-prava.png');
    this.load.image('witchs-house','Assets/witchs-house-m.png');
    this.load.image('rajcata','Assets/rajcata.png');
    this.load.image('buk','Assets/buk.png');
    this.load.image('borovice','Assets/borovice.png');
    this.load.image('vrba','Assets/vrba.png');
    this.load.image('jasan','Assets/jasan.png');
    this.load.image('mask', 'Assets/pancir-icon.png');
    this.load.image('zaslepkastraznice','Assets/zaslepka-straznice.png');
    this.load.image('zaslepkahornicela','Assets/zaslepkahornicela.png');
    this.load.image('zaslepkaspodnicela','Assets/zaslepkaspodnicela.png');
    this.load.image('studna','Assets/studna.png');
    this.load.image('stoly','Assets/stoly.png');
    //this.load.image('tma','Assets/tma.png');
    this.load.tilemapCSV('map', 'Assets/tiledmapOfSulan9.csv');
    this.load.spritesheet('player',  'Assets/player-spriteshet3-m.png', { frameWidth: 96, frameHeight: 138 });
    this.load.spritesheet('senkyrka',  'Assets/senkyrka-m.png', { frameWidth: 96, frameHeight: 138 });
    this.load.spritesheet('localman',  'Assets/localman-m.png', { frameWidth: 100, frameHeight: 138 });
    this.load.spritesheet('watchman',  'Assets/watchman.png', { frameWidth: 100, frameHeight: 138 });
    this.load.spritesheet('pigeonman',  'Assets/Holubář.png', { frameWidth: 69, frameHeight: 138 });
}

function create ()
{
    // When loading a CSV map, make sure to specify the tileWidth and tileHeight
    this.add.image(0,0,'map-part1').setOrigin(0);
    this.add.image(3600,0,'map-part2').setOrigin(0);
    this.add.image(7200,0,'map-part3').setOrigin(0);
    this.add.image(0,3600,'map-part4').setOrigin(0);
    this.add.image(3600,3600,'map-part5').setOrigin(0);
    this.add.image(7200,3600,'map-part6').setOrigin(0);
    this.add.image(0,7200,'map-part7').setOrigin(0);
    this.add.image(3600,7200,'map-part8').setOrigin(0);
    this.add.image(7200,7200,'map-part9').setOrigin(0);

    
    map = this.make.tilemap({ key: 'map', tileWidth:8, tileHeight: 8 });
    var tileset = map.addTilesetImage('bariera');
    var layer = map.createStaticLayer(0, tileset, 0, 0);

    

    doors = this.physics.add.staticGroup();
    door1 = doors.create(5036,2352,'door').setOrigin(0);
    door2 = doors.create(7975,3517,'door').setOrigin(0);
    door3 = doors.create(5550,2180,'door').setOrigin(0);
    door4 = doors.create(4954,4861,'door').setOrigin(0);
    door5 = doors.create(6696,2458,'door').setOrigin(0);
    door6 = doors.create(5626,4305,'door').setOrigin(0);
    door7 = doors.create(5432,4295,'door').setOrigin(0);
    door8 = doors.create(1807,2039,'door').setOrigin(0);
    door9 = doors.create(6038,4357,'door').setOrigin(0);
    door10 = doors.create(6036,4576,'door').setOrigin(0);
    door11 = doors.create(6037,4739,'door').setOrigin(0);
    door9.setScale(0.25);
    door10.setScale(0.25);
    door11.setScale(0.25);

    getout = this.physics.add.staticGroup();
    getout.create(5036,2653,'door').setOrigin(0);
    getout.create(5556,2388,'door').setOrigin(0);
    getout.create(7988,4234,'door').setOrigin(0);
    getout.create(4968,5044,'door').setOrigin(0);
    getout.create(6696,2650,'door').setOrigin(0);
    getout.create(1625,2028,'door').setOrigin(0);

    keyA = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.A);
    keyW = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.W);
    keyD = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.D);
    keyS = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.S);
    keyX = this.input.keyboard.addKey(Phaser.Input.Keyboard.KeyCodes.X);
    this.anims.create({
        key: 'idle',
        frames: this.anims.generateFrameNumbers('player', { start: 31, end: 40}),
        frameRate: 8,
        repeat: -1
    });
    this.anims.create({
        key: 'left',
        frames: this.anims.generateFrameNumbers('player', { start: 0, end: 13}),
        frameRate: 20,
        repeat: -1
    });
    this.anims.create({
        key: 'right',
        frames: this.anims.generateFrameNumbers('player', { start: 0, end: 13 }),
        frameRate: 20,
        repeat: -1
    });
    this.anims.create({
        key: 'up',
        frames: this.anims.generateFrameNumbers('player', { start: 14, end: 30 }),
        frameRate: 20,
        repeat: -1
    });
    this.anims.create({
        key: 'down',
        frames: this.anims.generateFrameNumbers('player', { start: 14, end: 30 }),
        frameRate: 24,
        repeat: -1
    });
     this.anims.create({
        key: 'localmanrunleft',
        frames: this.anims.generateFrameNumbers('localman', { start: 1, end: 10 }),
        frameRate: 16,
        repeat: -1
    });
       this.anims.create({
        key: 'localmanrundown',
        frames: this.anims.generateFrameNumbers('localman', { start: 12, end: 22 }),
        frameRate: 16,
        repeat: -1
    });
       this.anims.create({
        key: 'localmanstay',
        frames: [ { key: 'localman', frame: 0 } ]

    });
    
    npc = this.physics.add.staticGroup();
    senkyrka = this.physics.add.sprite(5417, 2220, 'senkyrka', 1);
    localman = this.physics.add.sprite(5800, 2800, 'localman', 1);
    watchman = this.physics.add.sprite(5761, 4658, 'watchman', 1);
    watchman2 = this.physics.add.sprite(9781, 2700, 'watchman', 1);
    watchman3 = this.physics.add.sprite(9794, 2830, 'watchman', 1);
    pigeonman = this.physics.add.sprite(7503,3723, 'pigeonman', 1);
    npc.add(senkyrka);
    npc.add(localman);
    npc.add(watchman);
    npc.add(watchman2);
    npc.add(watchman3);
    npc.add(pigeonman);

    /*rotationBall = this.add.image(6000,3000,'rotace').setOrigin(-5,0);
    rotationBall.setScale(0.3);*/

    zaslepkavrchnicela = this.add.image(5869,4081,'zaslepkahornicela').setOrigin(0);
    zaslepkaspodnicela = this.add.image(5859,4628,'zaslepkaspodnicela').setOrigin(0);
    zaslepkastraznice = this.add.image(5585,4168,'zaslepkastraznice').setOrigin(0);
    player = this.physics.add.sprite(6000,3000, 'player', 1);
    
    items = this.physics.add.staticGroup();
    panciricon = items.create(4674,4380,'pancir-icon');

    stoly = this.add.image(4610,2280,'stoly').setOrigin(0);

    hospodafasada = this.add.image(4557,1523,'hospodafasada').setOrigin(0);
    hospodafasadaprava = this.add.image(5311,1505,'pravafasada').setOrigin(0);
    witchhouse = this.add.image(4968,8940,'witchs-house');
    postoffice = this.add.image(6965,3189,'post-office').setOrigin(0);
    kovarna = this.add.image(2464,2370,'kovarna').setOrigin(0);
    statek = this.add.image(652,640,'farm').setOrigin(0);
    stodola = this.add.image(1762,1516,'stodola').setOrigin(0);
    bakeryfasade = this.add.image(6600,1228,'bakeryfasade').setOrigin(0);
    radnicefasada = this.add.image(4236,3177,'radnicefasada').setOrigin(0);
    studna = this.add.image(5334,2927,'studna').setOrigin(0);

    this.add.image(2674,339,'buk').setOrigin(0);
    this.add.image(6068,569,'buk').setOrigin(0);
    this.add.image(3227,2415,'buk').setOrigin(0);
    this.add.image(1418,6763,'buk').setOrigin(0);
    this.add.image(9066,7679,'buk').setOrigin(0);
    this.add.image(7914,631,'borovice').setOrigin(0);
    this.add.image(3152,1010,'borovice').setOrigin(0);
    this.add.image(8464,980,'borovice').setOrigin(0);
    this.add.image(3152,6410,'borovice').setOrigin(0);
    this.add.image(9616,8906,'borovice').setOrigin(0);
    this.add.image(1394,9143,'borovice').setOrigin(0);
    this.add.image(8216,2549,'jasan').setOrigin(0);
    this.add.image(502,3940,'jasan').setOrigin(0);
    this.add.image(5029,265,'jasan').setOrigin(0);
    this.add.image(4334,4765,'jasan').setOrigin(0);
    this.add.image(5982,8973,'jasan').setOrigin(0);
    this.add.image(846,7593,'jasan').setOrigin(0);
    this.add.image(4006,7557,'jasan').setOrigin(0);
    this.add.image(5568,8652,'vrba').setOrigin(0);
    this.add.image(10000,7284,'vrba').setOrigin(0);
    this.add.image(7728,6002,'vrba').setOrigin(0);
    this.add.image(5075,6605,'vrba').setOrigin(0);
    this.add.image(4437,9511,'rajcata').setOrigin(0);

    //var tma = this.add.image(760,500,'tma').setScrollFactor(0);
    //tma.visible = false;
    //this.add.image(0,0,'base').setOrigin(0);
    // Set up the player to collide with the tilemap layer. Alternatively, you can manually run
    // collisions in update via: this.physics.world.collide(player, layer).
    this.physics.add.collider(player, layer);
    dia1 = this.physics.add.overlap(player, senkyrka, diaSenk);
    dia2 = this.physics.add.overlap(player, localman, meetup);
    dia3 = this.physics.add.overlap(player, pigeonman, diapigeon);
    //boj1 = this.physics.add.overlap(player, watchman, faith1);
    //this.physics.add.collider(player, doors);
    
    this.physics.add.overlap(player, door1, hospodaplay);
    this.physics.add.overlap(player, door3, hospodaplay);
    this.physics.add.overlap(player, door4, radniceplay);
    this.physics.add.overlap(player, door5, bakeryplay);
    this.physics.add.overlap(player, door6, vezeniplay);
    this.physics.add.overlap(player, door7, vezenistop);
    this.physics.add.overlap(player, door8, stodolaplay);
    this.physics.add.overlap(player, door9, hornicelaplay);
    this.physics.add.overlap(player, door10, schovcelyplay);
    this.physics.add.overlap(player, door11, dolnicelaplay);
    this.physics.add.overlap(player, door2, rozhledna);
    this.physics.add.overlap(player, getout, getoutfce);
    this.physics.add.collider(player, panciricon, sberpancir);


    this.tweens.add({
        targets: localman,
        x: 1500,
        y: 3300,
        delay: 5000,
        duration: 8000
    });
    cam = this.cameras.main;

    cam.setBounds(0, 0,10800, 10800);
    cam.startFollow(player);

    zmenpatro = map.setCollisionBetween(0,1);
  
    cursors = this.input.keyboard.createCursorKeys();
    cam.setZoom(0.5);
}

function update ()
{
/*rotationBall.x = player.x;
rotationBall.y = player.y;
rotationBall.rotation +=0.02;*/
zvysovaniY();
if(meetlocalmen<1 && meetsenkyrka == 1){
    followean(localman);
}else{
    localman.setVelocity(0);
}

if(visithospoda==1 && meetsenkyrka ==0){
    ctrl = false;
    cutscene = true;
    player.flipX="true";
    this.tweens.add({
        targets: player.anims.play('left', true),
        x: 4700,
        y: 2470,
        delay: 300,
        duration: 1000
    });
    setTimeout(closecutscene,1300);
  this.tweens.add({
        targets: senkyrka,
        x: 4770,
        y: 2445,
        delay: 3000,
        duration: 1600
    });
  document.getElementById("localmanfirst").innerHTML = "„Pozdraven buď Jardel, náš dobrotivý Pán, vy jste Abraxián. Chci se zeptat jestli jste tu služebně. Potřeboval bych se zbavit jednoho Strážce.";
draw();
}
/*else{
    cutscene = false;
}*/


/*if(visitvezeni==1){
    followean(watchman);
}
else{
    watchman.setVelocity(0);
}
*/

if(ctrl==true){
    // Horizontal movement
    if (cursors.left.isDown||keyA.isDown)
    {
        player.flipX="true";
        player.body.setVelocityX(-750);
        player.anims.play('left', true);
        
    }
    else if (cursors.right.isDown||keyD.isDown)
    {
        player.body.setVelocityX(750);
        player.resetFlip();
        player.anims.play('right', true);
        
    }

    // Vertical movement
    else if (cursors.up.isDown||keyW.isDown)
    {
        player.body.setVelocityY(-500);
        player.resetFlip();
        player.anims.play('up', true);
        
    }
    else if (cursors.down.isDown||keyS.isDown)
    {
        player.resetFlip();
        player.body.setVelocityY(500);
        player.anims.play('down', true);
        
    }
    else {
        player.body.setVelocity(0);
        player.resetFlip();
        player.anims.play('idle', true);
    }
}

else if(cutscene == false){
        player.body.setVelocity(0);
        player.resetFlip();
        player.anims.play('idle', true);
    }

        
}
function followean(p1){
    var promenaX = Math.pow((300*300)-(promenaY*promenaY),0.5);
    var PX = player.x - (promenaX * zlom);
    var PY = player.y - promenaY;
    var LX = p1.x;
    var LY = p1.y;
    var zeroX =PX - LX;
    var zeroY =PY - LY;
    var absX = Math.abs(zeroX);
    var absY = Math.abs(zeroY);
    var VX = (zeroX/400)*500;
    var VY = (zeroY/400)*450;
    setTimeout(p1.setVelocityX(VX),50);
    setTimeout(p1.setVelocityY(VY),50);
    if(p1==localman){
        if(absX > absY){
            localman.anims.play('localmanrunleft',true);
        }
        else {
            localman.anims.play('localmanrundown',true);
        }
        if (zeroX > 0){
            localman.flipX="true";
        }
        else{
             localman.resetFlip();
        }
    }

}
function rozhledna(){
    cam.zoomTo(0.15,1000)
}

function schovFasadu(p1){
    p1.visible = false;
    cam.zoomTo(1,1000);
}

function hospodaplay(player,door1){
    schovFasadu(hospodafasada);
    visithospoda += 1;
}

function stodolaplay(player,door8){
    schovFasadu(stodola);
}
function radniceplay(player,door4){
    schovFasadu(radnicefasada);
    
}
function bakeryplay(player,door5){
    schovFasadu(bakeryfasade);
}


function vezeniplay(player,door5){

    zaslepkastraznice.visible = false;
    visitvezeni = 1;
}
function dolnicelaplay(player,door11){
    zaslepkaspodnicela.visible = false;
}
function hornicelaplay(player,door9){
    zaslepkavrchnicela.visible = false;
}
function schovcelyplay(player,door10){
    zaslepkaspodnicela.visible = true;
    zaslepkavrchnicela.visible = true;
}

function vezenistop(player,door7){
    zaslepkastraznice.visible = true;
}
function getoutfce(player, getout){
    hospodafasada.visible = true;
    radnicefasada.visible = true;
    bakeryfasade.visible = true;
    stodola.visible = true;
    cam.zoomTo(0.5,1000);

}
function closecutscene(){
    cutscene = false;
    draw();
}
/*===============================Dialogové funkce======================*/
function meetup(player,localman){
    meetlocalmen += 1;
    diaclick('2odst','4odst');
    localman.anims.play('localmanstay',true);
    dia2.active = false;
}
function diaSenk(player, senkyrka){
    diaclick('2odst','1odst');
    dia1.active = false;
}
function diapigeon(player, pigeonman){
    diaclick('2odst','18odst');
    dia3.active = false;
}
function diaclick(p1,p2){
    cam.zoomTo(1,1000);
    document.getElementById(p1).style='visibility: hidden;';
    document.getElementById(p2).style='visibility: visible;';
    ctrl = false;
    
}
function closeDial(p1,p2){
    cam.zoomTo(1,1000);
    document.getElementById(p1).style='visibility: hidden;';
    p2.active = true;
    ctrl = true;
}

function zvysovaniY(){
    if(zlom == -1 && promenaY < 300){
        promenaY += 3;
    }
    else{
        zlom = 1;
    }
    if(zlom == 1 && promenaY > -300){
        promenaY -= 3;
    }
    else{
        zlom = -1;
    }
//document.getElementById('box-money').innerHTML += promenaY+"<br>";
}
/*===============================Sbíraní-předmětů============================*/
function sberpancir(player, panciricon){
    obran('Pancíř',2);
    odev('pancir2.png',4);
    panciricon.destroy();
}
/*==============================Soubojový systém==============================*/
//Nutně změnit
function faith1(){
    var nahodaX = Math.floor(Math.random() * 1000);
    var nahodaY = Math.floor(Math.random() * 1000);  
    var odskokX = player.x + (500 - nahodaX);
    var odskokY = player.y + (500 - nahodaY);
    
    if(keyX.isDown){
        zmenPolohu(watchman, odskokX, odskokY);
    }
    else{
        zmenPolohu(watchman, odskokX, odskokY);
        hp -= 1; 
        draw();
    }
    
   
}
/*==============================Herní mechaniky===============================*/
function menu(p1){
    document.getElementById('box-postava').style='visibility: hidden;';
    document.getElementById('box-zasoba').style='visibility: hidden;';
    document.getElementById('box-money').style='visibility: hidden;';
    document.getElementById('box-ukoly').style='visibility: hidden;';
    document.getElementById(p1).style='visibility: visible;';
    document.getElementById('closebutton1').style='visibility: visible;';
} 
function draw(){
    document.getElementById('ViewHP').innerHTML = hp;
    document.getElementById('ViewEX').innerHTML = exp;
    document.getElementById('MyHp').innerHTML = hp;
    document.getElementById('MyExp').innerHTML = exp;
    document.getElementById('ViewZL').innerHTML = money;
    document.getElementById('KonZus').innerHTML = money;
    document.getElementById('faith').innerHTML = dogma;
    document.getElementById('cutscena').innerHTML = cutscene;
    document.getElementById('ctrl').innerHTML = ctrl;
}
function zmenPolohu(p1,p2,p3){
    var osoba = p1;
    osoba.x = p2;
    osoba.y = p3;
}
function prejdi(){
      this.tweens.add({
        targets: senkyrka,
        x: 5417,
        y: 2220,
        delay: 0,
        duration: 1600
    });
}
function closebutton(){
    document.getElementById('box-postava').style='visibility: hidden;';
    document.getElementById('box-zasoba').style='visibility: hidden;';
    document.getElementById('box-money').style='visibility: hidden;';
    document.getElementById('closebutton1').style='visibility: hidden;';
}
function mesec(p1,p2){
        if (p2<money*-1) {
        alert("Nemáš prachy!");
        event.stopPropagation();
        this.click(false);
            }
        else{
        var zapis = "<tr><td>"+p1+"</td><td>"+p2+"</td></tr>";
        document.getElementById("mesec").innerHTML += zapis;
        money = money + p2;
        if(p2>0){
            var audio = new Audio('Assets/_zvuky/Coin_Drop-Willem_Hunt.mp3');
            audio.play();
        }
        else{
            var audio = new Audio('Assets/_zvuky/Drop-Metal-Thing.mp3');
            audio.play();
                }
            }
        draw();
}
function dogmascore(p1){
    dogma = dogma + p1;
    draw();
    }
function quest(p1){
    var text = "<li>"+p1+"</li>"; 
    document.getElementById("tasks").innerHTML += text;
}
function kostka1(){
    var nahoda = Math.floor(Math.random() * 10); 
    if(nahoda>3){
        document.getElementById("odpoved").innerHTML = "<span class='actor'>Vypravěč:</span>Očividně se cítí polichocena, a trochu zčervenala. Za chvíli ti donese pivo. A pod pivem je lísteček. Na něm stojí aby se dosatvil k zadním vrátkům hostince, hodinu před půlnocí."
    }
    else{
        document.getElementById("odpoved").innerHTML = "<span class='actor'>šenkýřka naštvaně:</span>„Co si to dovolujete. Okamžitě opuste tento hostinec. Toto je slušný podnik."
        document.getElementById("choice1").style = "display:none";
    }
}

function obran(p1,p2){
    var ochran = "<tr><td>"+p1+"</td><td>"+p2+"</td></tr>"
    document.getElementById("obrana").innerHTML += ochran;
    obrana = obrana + p2;
}
function odev(p1,p2){
    var load = "<img src=\""+p1+"\" style=\"z-index:"+p2+";\">";
    document.getElementById("vizual").innerHTML += load;
}